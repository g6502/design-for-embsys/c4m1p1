library ieee;
use ieee.std_logic_1164.all;

entity C4M1P1 is port
	(
		SW		: IN STD_LOGIC_VECTOR (9 downto 0);
		HEX0 	: OUT STD_LOGIC_VECTOR (7 downto 0);
		HEX1 	: OUT STD_LOGIC_VECTOR (7 downto 0)
	);
end entity C4M1P1;

architecture implementation of C4M1P1 is 

signal NCODE0	: std_logic_vector(7 downto 0);
signal NCODE1	: std_logic_vector(7 downto 0);
signal SW0 		: std_logic_vector(3 downto 0) := SW(3 downto 0);
signal SW1 		: std_logic_vector(3 downto 0) := SW(7 downto 4);

begin
	process (SW)
	begin
		case SW0 is 
			when "0000" =>
				NCODE0 <= "11000000";
			when "0001" =>
				NCODE0 <= "11111001";
			when "0010" =>
				NCODE0 <= "10100100";
			when "0011" =>
				NCODE0 <= "10110000";
			when "0100" =>
				NCODE0 <= "10011001";          
			when "0101" =>
				NCODE0 <= "10010010";
			when "0110" =>
				NCODE0 <= "10000010";
			when "0111" =>
				NCODE0 <= "11111000";
			when "1000" =>
				NCODE0 <= "10000000";
			when "1001" =>
				NCODE0 <= "10011000";
			when others =>
				NCODE0 <= "10001000";
--			when "1011" =>
--				NCODE0 <= X"1F";
--			when "1100" =>
--				NCODE0 <= X"4E";
--			when "1101" =>
--				NCODE0 <= X"3D";
--			when "1110" =>
--				NCODE0 <= X"4F";
--			when "1111" =>
--				NCODE0 <= X"47";
		end case;
		HEX0 <= NCODE0;
		case SW1 is 
			when "0000" =>
				NCODE1 <= "11000000";
			when "0001" =>
				NCODE1 <= "11111001";
			when "0010" =>
				NCODE1 <= "10100100";
			when "0011" =>
				NCODE1 <= "10110000";
			when "0100" =>
				NCODE1 <= "10011001";          
			when "0101" =>
				NCODE1 <= "10010010";
			when "0110" =>
				NCODE1 <= "10000010";
			when "0111" =>
				NCODE1 <= "11111000";
			when "1000" =>
				NCODE1 <= "10000000";
			when "1001" =>
				NCODE1 <= "10011000";
			when others =>
				NCODE1 <= "10001000";
--			when "1011" =>
--				NCODE1 <= X"1F";
--			when "1100" =>
--				NCODE1 <= X"4E";
--			when "1101" =>
--				NCODE1 <= X"3D";
--			when "1110" =>
--				NCODE1 <= X"4F";
--			when "1111" =>
--				NCODE1 <= X"47";
		end case;
		HEX1 <= NCODE1;
	end process;
end architecture implementation;

